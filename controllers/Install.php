<?php namespace App\Controllers;

use App\Classes\TableItem;

class Install extends \App\Abstracts\Controller
{
    public function index()
    {
        if( !db()->hasTable('tab_users') ) {
            $prefix = db()->prefix();
            
            db()->exec("
                CREATE TABLE IF NOT EXISTS `{$prefix}users` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `email` varchar(255) NOT NULL DEFAULT '',
                  `password` varchar(64) NOT NULL DEFAULT '',
                  `name` varchar(255) NOT NULL DEFAULT '',
                  `session` varchar(48) NOT NULL DEFAULT '',
                  `token` varchar(48) NOT NULL DEFAULT '',
                  `ip` varchar(15) NOT NULL DEFAULT '',
                  `stamp` bigint(20) unsigned NOT NULL DEFAULT '0',
                  `created_at` timestamp NULL DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `email` (`email`),
                  KEY `password` (`password`),
                  KEY `session` (`session`),
                  KEY `token` (`token`),
                  KEY `ip` (`ip`),
                  KEY `stamp` (`stamp`),
                  KEY `created_at` (`created_at`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ");
            
            db()->reloadTables();
            
            $user = new TableItem('tab_users');
            $user->name       = 'User';
            $user->email      = 'user@test.local';
            $user->password   = password_crypt('test');
            $user->created_at = date('Y-m-d H:i:s');
            $user->stamp      = time();
            
            $user->save();
            
            return $this->view('install/success'); 
        }
        
        return $this->view('install/fail');
    }
}
