<?php namespace App\Controllers;

class Index extends \App\Abstracts\Controller
{
    /**
    * Метод по умолчанию
    */
    public function index()
    {
        return $this->view('index/index');
    }
    
    public function register()
    {
        if( !is_ajax() ) {
            exit('Некорректный запрос...');
        }
        
        if( auth()->check() ) {
            return $this->json([
                'status' => 'success',
                'userId' => auth()->id(),
            ]);
        }
        
        $data = input()->data;

        if( empty($data) ) {
            return $this->json([
                'status' => 'error',
                'data'   => [
                    'errorText' => 'Нет данных для обработки...',
                ],
            ]);
        }
        
        if( !($json = json_decode($data)) ) {
            return $this->json([
                'status' => 'error',
                'data'   => [
                    'errorText' => 'Некорректные данные для обработки...',
                ],
            ]);
        }
        
        if( empty($json->login) || !is_email($json->login) ) {
            return $this->json([
                'status' => 'error',
                'data'   => [
                    'errorText' => 'Указан некорректный Email...',
                ],
            ]);
        }
        
        if( empty($json->name) || !is_name($json->name) ) {
            return $this->json([
                'status' => 'error',
                'data'   => [
                    'errorText' => 'Указано некорректное Имя...',
                ],
            ]);
        }
        
        if( empty($json->pwd1) || empty($json->pwd2) || $json->pwd1 != $json->pwd2 ) {
            return $this->json([
                'status' => 'error',
                'data'   => [
                    'errorText' => 'Проверьте правильность пароля...',
                ],
            ]);
        }
        
        $user = db('tab_users', [
            'email' => $json->login,
        ]);
        
        if( !empty($user->id) ) {
            return $this->json([
                'status' => 'error',
                'data'   => [
                    'errorText' => 'Пользователь с таким Email адресом уже зарегистрирован',
                ],
            ]);
        }
        
        $user             = db('tab_users');
        $user->name       = $json->name;
        $user->email      = $json->login;
        $user->password   = password_crypt($json->pwd1);
        $user->created_at = date('Y-m-d H:i:s');
        $user->save();
        
        if( !empty($user->id) && auth()->signin($user->email, $json->pwd1) ) {
            return $this->json([
                'status' => 'success',
                'userId' => auth()->id(),
            ]);
        }
        
        return $this->json([
            'status' => 'error',
            'data'   => [
                'errorText' => 'Регистрация не удалась...',
            ],
        ]);
    }
    
    /**
    * Метод авторизации
    */
    public function login()
    {
        if( !is_ajax() ) {
            exit('Некорректный запрос...');
        }
        
        if( auth()->check() ) {
            return $this->json([
                'status' => 'success',
                'userId' => auth()->id(),
            ]);
        }

        $data = input()->data;

        if( empty($data) ) {
            return $this->json([
                'status' => 'error',
                'data'   => [
                    'errorText' => 'Нет данных для обработки...',
                ],
            ]);
        }
        
        if( !($json = json_decode($data)) || empty($json->login) || empty($json->password) ) {
            return $this->json([
                'status' => 'error',
                'data'   => [
                    'errorText' => 'Некорректные данные для обработки...',
                ],
            ]);
        }
        
        if( auth()->signin($json->login, $json->password) ) {
            if( !empty($json->rememberme) ) {
                $token       = uuid();
                $user        = auth()->user();
                $user->token = $token;
                $user->save();
                
                cookies()->token = $token;
            }
            
            return $this->json([
                'status' => 'success',
                'userId' => auth()->id(),
            ]);
        }
        
        return $this->json([
            'status' => 'error',
            'data'   => [
                'errorText' => 'Логин или пароль указан не правильно...',
            ],
        ]);
    }
    
    public function logout()
    {
        if( auth()->check() ) {
            auth()->signout();
            cookies()->token = null;
        }
        
        return $this->json([
            'status' => 'success',
        ]);
    }
}
