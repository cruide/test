<?php namespace App\Classes;

  use App\Traits\SingletonTrait;
  use App\Traits\RegistryTrait;

  final class Input
  {
      use SingletonTrait;
      use RegistryTrait;

      private $params;

// -------------------------------------------------------------------------------------
      public function __construct()
      {
          global $_GET, $_POST, $_SERVER;

          if( isset($_POST) && array_count($_POST) > 0 ) {
              foreach($_POST as $key=>$val) {
                  $this->_properties[ $this->_clean_key($key) ] = $this->_clean_val($val);
              }
          }

          if( isset($_GET) && array_count($_GET) > 0 ) {
              foreach($_GET as $key=>$val) {
                  $this->params[ $this->_clean_key($key) ] = $this->_clean_val($val);
              }
          }
      }
// -------------------------------------------------------------------------------------
      public function getParam( $name )
      {
          if( array_key_isset($name, $this->params) ) {
              return $this->params[ $name ];
          }
          
          return null;
      }
// -------------------------------------------------------------------------------------
      public function getParams()
      {
          return $this->params;
      }
// -------------------------------------------------------------------------------------
      protected function getClean( $name = null )
      {
          return isset($this->_properties[ $name ]) ? $this->_xss_clean( $this->_properties[$name] ) : null;
      }
// -------------------------------------------------------------------------------------
      protected function _clean_key($str)
      {
          if( !preg_match("/^[a-z0-9:_\\/-]+$/i", $str) ) {
              abort("Your request {$str} contains disallowed characters.");
          }

          return $str;
      }
// -------------------------------------------------------------------------------------
      protected function _clean_val($str)
      {
          if( is_array($str) ) {
              $_array = [];

              foreach($str as $key=>$val) {
                  $_array[ $this->_clean_key($key) ] = $this->_clean_val($val);
              }

              return $_array;
          }

          if( get_magic_quotes_gpc() ) {
              $str = stripslashes($str);
          }

          return preg_replace("/\015\012|\015|\012/", "\n", $str);
      }
// -------------------------------------------------------------------------------------
      protected function _xss_clean($data)
      {
          if( is_array($data) ) {
              $_ = [];

              foreach($data as $key=>$val) {
                  $_[ $key ] = $this->_xss_clean($val);
              }

              return $_;
          }

          // Fix &entity\n;
          $data = str_replace(['&amp;','&lt;','&gt;'], ['&amp;amp;','&amp;lt;','&amp;gt;'], (string)$data);
          $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
          $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
          $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

          // Remove any attribute starting with "on" || xmlns
          $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

          // Remove javascript: && vbscript: protocols
          $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
          $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
          $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

          // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
          $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
          $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
          $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

          // Remove namespaced elements (we do not need them)
          $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

          do {
              // Remove really unwanted tags
              $old_data = $data;
              $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
          } while( $old_data !== $data );

          // we are done...
          return $data;
      }
  }

