<?php namespace App\Classes;
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2017 Alexander Tishchenko
*/

if( !extension_loaded('pdo_mysql') ) { 
    trigger_error( 'MySQL PDO extension not found...', E_USER_ERROR );
}

use App\Traits\SingletonTrait;

// -------------------------------------------------------------------------------------
class PDB
{
    use SingletonTrait;

    protected $_tables;
    protected $_pdo; 
    protected $_statement;
    protected $_cfg;
    protected $_fields_cache;
    protected $_keys_cache;
    protected $_connected;
    protected $_mysql_version;
        
    private static $_query_logs;
    private static $_big_query_logs;
        
    const ERROR   = E_USER_ERROR;
    const WARNING = E_USER_WARNING;
    const NOTICE  = E_USER_NOTICE;
    
    const BIG_QUERY_TIME = 1;
    
// -------------------------------------------------------------------------------------
    public function __construct()
    {
        if( self::$_query_logs == null ) {
            self::$_query_logs = array();
        }

        if( self::$_big_query_logs == null ) {
            self::$_big_query_logs = array();
        }
        
        $this->_fields_cache = array();
        $this->_connected    = false;

        $conf = cfg('dbase');

        $this->_cfg = array(
            'host'     => 'localhost',
            'dbase'    => 'test',
            'user'     => 'root',
            'password' => '',
            'port'     => 3306,
            'prefix'   => 'tab_',
        );
        
        $this->init($conf->default->host, 3306, $conf->default->prefix);
        $this->connect($conf->default->database, $conf->default->username, $conf->default->password);
    }
// -------------------------------------------------------------------------------------
    /**
    * Return base name used
    * 
    */
    public function getBaseName()
    {
        return (isset($this->_config['dbase'])) ? $this->_config['dbase'] : false;
    }
// -------------------------------------------------------------------------------------
    public function init($host = 'localhost', $port = 3306, $prefix = 'db_')
    {
        $this->_config['host']   = $host;
        $this->_config['port']   = $port;
        $this->_config['prefix'] = $prefix;
        
        return $this;
    }
// -------------------------------------------------------------------------------------
    public function connect($dbase, $user = 'root', $password = '')
    {
        if( !isset($dbase) ) {
            return false;
        }
        
        $this->_config['dbase']    = $dbase;
        $this->_config['user']     = $user;
        $this->_config['password'] = $password;
        
		try {
			$this->_pdo = new \PDO(
                "mysql:host={$this->_config['host']};" .
                "port={$this->_config['port']};" .
                "dbname={$this->_config['dbase']}", 

                $this->_config['user'], 
                $this->_config['password']
            );
		} catch( \PDOException $e ) {
			abort('Unable to connect to the MySQL database. ' . $e->getMessage());
		}
        
        $this->_connected = true;
        
        $this->_pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->_pdo->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        
        $this->exec('SET character_set_database = utf8');
        $this->exec('SET NAMES utf8');

        $this->reloadTables();
        
        return $this->_connected;
    }
// -------------------------------------------------------------------------------------
    public function isConnected()
    {
        return $this->_connected;
    }
// -------------------------------------------------------------------------------------
    public function error( \Exception $e )
    {
        abort( $e->getMessage() );
    }
// -------------------------------------------------------------------------------------
    public function exec( $request )
    {
        if( $this->_connected && isset($request) ) {
            $unixtime = time();
            
            $_     = $this->_pdo->exec( $request );
            $error = $this->_pdo->errorInfo();
            
            if( !empty($error[2]) ) {
                throw new \PDOException( $error[2] );
            }
            
            if( (time() - $unixtime) >= self::BIG_QUERY_TIME ) {
                $this->_addBigRequestLog( $request );
            }
            
            $this->_addRequestLog( $request );
            
            return $_;
        }
    }
// -------------------------------------------------------------------------------------
    public function query( $request, $fetch_type = null, $var1 = null, $var2 = null )
    {
        if( $this->_connected && isset($request) ) {
            try {
                $unixtime = time();
                
                if( $fetch_type === null ) {
                    $_ = $this->_pdo->query( $request );
                } else if( $fetch_type !== null && $var1 !== null ) {
                    $_ = $this->_pdo->query( $request, $fetch_type, $var1 );
                } else if($fetch_type !== null && $var1 !== null && $var !== null) {
                    $_ = $this->_pdo->query( $request, $fetch_type, $var1, $var2 );
                }
                
                if( (time() - $unixtime) >= self::BIG_QUERY_TIME ) {
                    $this->_addBigRequestLog( $request );
                }
                
            } catch( \PDOException $e ) {
                $this->error( $e );
            }
            
            $this->_addRequestLog( $request );
            
            return $_;
        }
    }
// -------------------------------------------------------------------------------------
    public function prefix()
    {
        return $this->_cfg['prefix'];
    }
// -------------------------------------------------------------------------------------
    /**
    * Reload tables names from DBase
    * 
    */
    public function reloadTables()
    {
        $tables = $this->prepare('SHOW TABLES FROM ' . $this->_config['dbase'] )
                       ->execute()
                       ->fetchAll();

        foreach($tables as $key=>$val) {
            $this->_tables[] = reset( $val );
        }
    }
// -------------------------------------------------------------------------------------
    public function serverVersion()
    {
      	if( empty($this->_mysql_version) ) {
            $ver = $this->prepare('SELECT VERSION()')->execute()->fetch();
			$this->_mysql_version = reset( $ver );
      	}
      	
        return $this->_mysql_version;
    }
// -------------------------------------------------------------------------------------
    /**
    * Check for table exists
    * 
    * @param string $table
    * @return bool
    */
    public function hasTable($table)
    {
        if( empty($table) ) {
            return false;
        }
        
        $table = $this->_correctTableName($table);

        if( array_count($this->_tables) > 0 ) {
            return in_array($table, $this->_tables);
        }
        
        return false;
    }
// -------------------------------------------------------------------------------------
    public function getPrefix()
    {
		return $this->_config['prefix'];
    }
// -------------------------------------------------------------------------------------
    public function getTablesList()
    {
        return $this->_tables;
    }
// -------------------------------------------------------------------------------------
    /**
    * Return fields list
    * 
    * @param string $table_name
    * @return array
    */
    public function getFieldsList($table_name, $_as_object = false)
    {
        if( empty($table_name) ) {
            return false;
        }

        $showed_table_name = $this->_correctTableName($table_name);
        
        if( !empty($this->_fields_cache[ $showed_table_name ]) 
        && is_array($this->_fields_cache[ $showed_table_name ]) ) 
        {
            return ($_as_object) ?
                $this->_arrayToObject( $this->_fields_cache[ $showed_table_name ] ) :
                $this->_fields_cache[ $showed_table_name ];
        }
        
        if( !$this->hasTable($table_name) ) {
            throw new \Exception(
                __METHOD__ . ' - Table `' . 
                $showed_table_name . '` on database `' . 
                $this->_config['dbase'] . '` not found'
            );
        }
        
        $_null = $this->query('SHOW FIELDS FROM ' . $showed_table_name)
                      ->fetchAll();
        
        if( empty($_null) ) {
            throw new \Exception(
                __METHOD__ . ' - Empty result for fields request into `' .
                $showed_table_name . '`',
                500
            );
        }
        
        foreach($_null as $key=>$val) {
            if( preg_match('/^enum\(/i', $val['Type']) ) {
                $val['Type'] = str_replace(array(
                        'enum(', 'ENUM(', ')', '\'', '"',
                    ), '', $val['Type']
                );

                $val['Type'] = explode(',', $val['Type']);
            } else if( preg_match('/^(varchar|text|char|tinytext|mediumtext|longtext)\(/i', $val['Type']) ) {
                $val['Type'] = 'string';
            } else if( preg_match('/^(int|bigint|tinyint|smallint|mediumint|bit)\(/i', $val['Type']) ) {
                $val['Type'] = 'integer';                                     
            } else if( preg_match('/^float\(/i', $val['Type']) ) {
                $val['Type'] = 'float';                                     
            } else if( preg_match('/^double\(/i', $val['Type']) ) {
                $val['Type'] = 'double';
            } else if( preg_match('/^decimal\(/i', $val['Type']) ) {
                $val['Type'] = 'decimal';
            }

            $this->_fields_cache[ $showed_table_name ][ $val['Field'] ] = $val['Type'];
        }

        return ($_as_object) ?
            $this->_arrayToObject( $this->_fields_cache[ $showed_table_name ] ) :
            $this->_fields_cache[ $showed_table_name ];
    }
// -------------------------------------------------------------------------------------
    /**
    * Return values of ENUM fields
    * 
    * @param string $table_name
    * @param string $field_name
    * @return bool/array
    */
    public function getEnumValues($table_name, $field_name) 
    {
        $showed_table_name = $this->_correctTableName($table_name);

        if( !isset($this->_fields_cache[ $showed_table_name ]) ) {
            $this->getFieldsList($table_name);
        }

        if( isset($this->_fields_cache[ $showed_table_name ][ $field_name ])
         && is_array($this->_fields_cache[ $showed_table_name ][ $field_name ]) )
        {
            return $this->_fields_cache[ $showed_table_name ][ $field_name ];
        }
        
        return false;
    }
// -------------------------------------------------------------------------------------
    /**
    * Return key field name of table
    * 
    * @param string $table_name
    * @return mixed
    */
    public function getPrimaryKey($table_name)
    {
        if( empty($table_name) ) {
            return false;
        }

        $real_table_name = $this->_correctTableName($table_name);
        
        if( !empty($this->_keys_cache[ $real_table_name ]) ) {
            return $this->_keys_cache[ $real_table_name ];
        }
        
        $_tmp = array();
        
        if( $this->hasTable($table_name) ) {
            $_tmp = $this->query("SHOW KEYS FROM {$real_table_name}")->fetchAll();
            
        } else {
            throw new \Exception(
                "MySQL_PDO::get_primary_key - Table `{$real_table_name}` on database `{$this->_config['dbase']}` not found"
            );
        }
        
        if( array_count($_tmp) ) {
            foreach($_tmp as $key=>$val) {
                if( $val['Key_name'] == 'PRIMARY' ) { 
                    $this->_keys_cache[ $real_table_name ] = $val['Column_name'];
                    return $val['Column_name'];
                }
            }
        }
         
        return false;
    }
// -------------------------------------------------------------------------------------
    public function request($query, $values = null)
    {
        return $this->prepare($query)->execute($values);
    }
// -------------------------------------------------------------------------------------
    public function requestOne($query, $values = null, $obj = false)
    {
        return $this->request($query, $values)->fetch($obj);
    }
// -------------------------------------------------------------------------------------
    public function requestAll($query, $values = null, $obj = false)
    {
        return $this->request($query, $values)->fetchAll($obj);
    }
// -------------------------------------------------------------------------------------
    public function prepare($request, $driver_options = array())
    {
        if( empty($request) ) {
            throw new \Exception(
                "MySQL_PDO::prepare - Empty request ({$request})"
            );
        }
        
        if( isset($this->_statement) ) {
            $this->_statement = null;
        }
        
        $request          = $this->_correctTableName($request);
        $this->_statement = $this->_pdo->prepare($request, $driver_options);
        
        return $this;
    }
// -------------------------------------------------------------------------------------
    public function execute( $data = array() )
    {
        $vars = array();

        if( array_count($data) > 0 ) {
            foreach($data as $key=>$val) {
//                if( $this->_isCorrectVariableName($key) && preg_match("/:{$key}/is", $this->_statement->queryString) ) {
                if( preg_match("/:{$key}/is", $this->_statement->queryString) ) {
                    $vars[ ':' . $key ] = $val;
                    
                } else {
                    $vars[] = $val;
                }
            }
        }

        if( $this->_statement instanceof \PDOStatement ) {
            try {
                $unixtime = time();
                
                $this->_statement->execute( $vars );

                if( (time() - $unixtime) >= self::BIG_QUERY_TIME ) {
                    $this->_addBigRequestLog( $request );
                }
                
            } catch( \PDOException $e ) {
                abort( $e->getMessage() );
            }

            $this->_addRequestLog( $this->_statement->queryString );    
                
            $error = (int)$this->_statement->errorCode();
            
            if( !empty($error) ) {
                $error = $this->_statement->errorInfo();
                $this->error( new \PDOException(
                    $error[2] . ' into ' . $this->_statement->queryString 
                ));
            }
            
        } else {
            throw new \Exception(
                'MySQL_PDO::execute - Not prepared to request'
            );
        }
        
        return $this;
    }
// -------------------------------------------------------------------------------------
    public function fetchAll($obj = false)
    {
        if( !($this->_statement instanceof \PDOStatement) ) {
            throw new \Exception(
                'nMySQL_PDO::fetch_all - Not prepared to request'
            );
        }
                      
        return $obj ? $this->_statement->fetchAll( \PDO::FETCH_CLASS, '\\App\\Classes\\stdObject' ) 
                    : $this->_statement->fetchAll( \PDO::FETCH_ASSOC );
    }
// -------------------------------------------------------------------------------------
    public function fetch($obj = false)
    {
        if( !($this->_statement instanceof \PDOStatement) ) {
            throw new \Exception(
                'nMySQL_PDO::fetch - Not prepared to request'
            );
        }

        return ($obj) ? $this->_statement->fetchObject('\\App\\Classes\\stdObject') 
                      : $this->_statement->fetch( \PDO::FETCH_ASSOC );
    }
// -------------------------------------------------------------------------------------
    /**
    * Get object by ID
    *
    * @param mixed $table
    * @param mixed $id
    * @param mixed $obj
    */
    public function find($table, $id, $obj = true)
    {
        $primary_key = $this->getPrimaryKey($table);
        
        return $this->prepare("SELECT * FROM `{$table}` WHERE `{$primary_key}` = ? LIMIT 1")
                    ->execute( array($id) )
                    ->fetch($obj);
    }
// -------------------------------------------------------------------------------------
    /**
    * Метод для выборки из БД
    * 
    * @param string $table
    * @param array $terms
    * @return array
    * 
    * Входящие данные
    * 
    * array(
    *     'fields' => array(),      - Перечень полей
    *     'where' => '',            - условие выбора /  `a` = 1 AND `b` = :data  /
    *     'values' => array(),      - если в условие есть ключи, то передаём array с данными как в execute() 
    *     'order' => array(),       - Описание сортировки array( `a` => 'ASC', `b` => 'DESC' )
    *     'limit' => '',            - Лимит записей. В случае передачи 1, будет использован fetch(). Аналог LIMIT 30
    *     'from' => '',             - От какой записи выбирать. Аналог LIMIT 30,30
    *     'obj'  => true,           - вернуть ьфссив объектов
    * )
    * 
    */
    public function select($table, $terms = array())
    {
        if( empty($terms) ) {
            return $this->selectAll($table);
        }

        if( !$this->hasTable($table) ) {
            return false;
        }

        $obj = !empty($terms['obj']) ? true : false;
        
        $fields_in_table = $this->getFieldsList($table);

        /**
        * Selected fields make
        */
        if( isset($terms['fields']) && is_array($terms['fields']) && count($terms['fields']) > 0 ) {
            $fields_selected = array();

            foreach($terms['fields'] as $key=>$val) {
                if( is_scalar($val) && array_key_isset($val, $fields_in_table) ) {
                    $fields_selected[] = "`{$val}`";
                }
            }

            $fields = implode(',', $fields_selected);
            unset($fields_selected, $key, $val);
        }

        $fields  = (empty($fields)) ? '*' : $fields;
        $request = "SELECT {$fields} FROM {$table}";

        if( !empty($terms['where']) ) {
            $request .= " WHERE ({$terms['where']})";
        }

        if( !empty($terms['group']) && array_key_isset($terms['group'], $fields_in_table) ) {
            $request .= " GROUP BY `{$terms['group']}`";
        }

        if( isset($terms['order']) && is_array($terms['order']) && count($terms['order']) > 0 ) {
            $order_selected = array();

            foreach($terms['order'] as $key=>$val) {
                if( array_key_isset($key, $fields_in_table) ) {
                    $sort = ($val == 'DESC') ? 'DESC' : 'ASC';
                    $order_selected[] = "`{$key}` {$sort}";
                }
            }

            $order = implode(', ', $order_selected);
            unset($order_selected, $key, $val, $sort);

            $request .= " ORDER BY {$order}";
        }

        if( isset($terms['limit']) && is_numeric($terms['limit']) && $terms['limit'] > 0 ) {
            $request .= ' LIMIT ' . ( (isset($terms['from']) && is_numeric($terms['from'])) ? "{$terms['from']}," : '' ) . $terms['limit'];
        }

        $this->prepare($request)->execute( (isset($terms['values']) && array_count($terms['values']) > 0) ? $terms['values'] : null );

        return ( isset($terms['limit']) && $terms['limit'] == 1 ) ? $this->fetch($obj) : $this->fetchAll($obj);
    }
// -------------------------------------------------------------------------------------
    protected function _makeWhere(array $where)
    {
        if( !empty($where) ) {
            foreach($where as $key=>$val ) {
                if( is_array($val) && isset($val['operator']) && array_key_isset('value', $val) ) {
                    return "`{$key}` {$val['operator']} " . (is_numeric($val['value']) ? $val['value'] : "'{$val['value']}'");
                }
            }
        }
        
        return null;
    }
// -------------------------------------------------------------------------------------
    public function selectAll($tab_name, $order = null, $obj = false)
    {
        if( !$this->hasTable($tab_name) ) return false;

        return $this->request( "SELECT * FROM `{$tab_name}`" . ((!empty($order)) ? " ORDER BY `{$order}`" : '') )
                    ->fetchAll($obj);
    }
// -------------------------------------------------------------------------------------
    /**
    * Получение количества записей с таблицы
    * 
    * Пример:
    * $where = '`id` = 5'
    * 
    * @param string $table
    * @param string $where
    * 
    * @return mixed
    */
    public function count($table, $where = null)
    {
        if( !$this->hasTable($table) ) {
            return false;
        }
        
        if( !empty($where) ) {
            $where = " WHERE ({$where})";
        }
        
        $_ = $this->requestOne("SELECT COUNT(*) AS `count` FROM `{$table}`{$where}");

        return (isset($_['count'])) ? (int)$_['count'] : false;
    }
// -------------------------------------------------------------------------------------
    public function insert($table, $fields)
    {
        if( !$this->hasTable($table) || !is_array($fields) ) {
            return false;
        }
        
        if( count($fields) == 0 ) {
            return false;
        }

        $table_fields = $this->getFieldsList($table);
        $sets = $values = array();

        foreach($fields as $key=>$val) {
            if( array_key_isset($key, $table_fields) ) {
                $sets[] = "`{$key}` = :{$key}";
                $values[ $key ] = $val;
            }
        }

        if( count($sets) > 0 ) {
            return $this->prepare("INSERT INTO `{$table}` SET " . implode(', ', $sets))
                        ->execute($values)
                        ->lastInsertId();
        }

        return false;
    }
// --------------------------------------------------------------------------------
    public function update($table, $properties, $where = null)
    {
        if( !$this->hasTable($table) || !is_array($properties) ) {
            return false;
        }
        
        if( count($properties) == 0 ) {
            return false;
        }

        $table_fields   = $this->getFieldsList($table);
        $primary_key    = $this->getPrimaryKey($table);
        $sets = $values = array();

        if( !array_key_isset($primary_key, $properties) && empty($where) ) {
            throw new \Exception('DB::update - Undefined primary key for updating for table `' . $table . '`');
        }
        
        if( empty($where) ) {
            $values['primarykey'] = $properties[ $primary_key ];
        }
        
        unset($properties[ $primary_key ]);

        foreach($properties as $key=>$val) {
            if( array_key_isset($key, $table_fields) ) {
                $sets[] = "`{$key}` = :{$key}";
                $values[ $key ] = $val;
            }
        }

        if( count($sets) > 0 ) {
            if( !empty($where) ) {
                $this->prepare("UPDATE `{$table}` SET " . implode(', ', $sets) . ' WHERE ' . $where)
                     ->execute($values);
                     
            } else {
                $this->prepare("UPDATE `{$table}` SET " . implode(', ', $sets) . ' WHERE `' . $primary_key . '` = :primarykey')
                     ->execute($values);
            }
                 
            return true;
        }

        return false;
    }
// --------------------------------------------------------------------------------
    public function delete($table, $id)
    {
        if( !$this->hasTable($table) ) {
            return false;
        }
        
        $primary_key = $this->getPrimaryKey($table);
        
        $this->request("DELETE FROM `{$table}` WHERE `{$primary_key}` = ?", array($id));
        
        return true;
    }
// -------------------------------------------------------------------------------------
    public function lastInsertId()
    {
        if( !($this->_pdo instanceof \PDO) ) {
            throw new \Exception(
                'nMySQL_PDO::last_insert_id - Class is not initialized'
            );
        }

        return $this->_pdo->lastInsertId();
    }
// -------------------------------------------------------------------------------------
    /**
    * Empty table function
    * 
    * @param string $table_name
    */
    public function emptyTable($table_name)
    {
        if( !empty($table_name) && $this->hasTable($table_name) ) {
            $this->prepare(
                'TRUNCATE ' . $this->_correctTableName($table_name)
            )->execute();
            
            return true;
        }
        
        return false;
    }
// -------------------------------------------------------------------------------------
    /**
    * Optimize table function
    * 
    * @param string $table_name
    */
    public function optimizeTable($table_name)
    {
        if(!empty($table_name) && $this->hasTable($table_name)) {
            $this->prepare(
                'OPTIMIZE TABLE ' . $this->_correctTableName($table_name)
            )->execute();
            
            return true;
        }
        
        return false;
    }
// -------------------------------------------------------------------------------------
    /**
    * Repair table function
    * 
    * @param string $table_name
    * @param bool $ext
    * @param bool $frm
    * 
    * @return bool
    */
    public function repairTable($table_name, $ext = false, $frm = false)
    {
        if( !empty($table_name) && $this->hasTable($table_name) ) {

            $_result = $this->prepare( 'REPAIR TABLE ' . $this->_correctTableName($table_name) . (($ext) ? ' EXTENDED' : '') . (($frm) ? ' FRM' : '') )
                            ->execute()
                            ->fetch();
            
            if($_result['Msg_text'] == 'Ok') {
                return true;
            }
            
            return false;
        }
        
        return false;
    }
// -------------------------------------------------------------------------------------
    /**
    * Check table function
    * 
    * @param string $table_name
    */
    public function checkTable($table_name, $ext = false)
    {
        if( !empty($table_name) && $this->hasTable($table_name) ) {

            $_result = $this->prepare( 'CHECK TABLE ' . $this->_correctTableName($table_name) . (($ext) ? ' EXTENDED' : '') )
                            ->execute()
                            ->fetch();
            
            if( $_result['Msg_text'] == 'Ok' ) {
                return true;
            }
            
            return false;
        }
        
        return false;
    }
// -------------------------------------------------------------------------------------
    /**
    * Return request log
    * 
    */
    public static function getLog()
    {
        return self::$_query_logs;
    }
// -------------------------------------------------------------------------------------
    /**
    * Return request log
    * 
    */
    public static function getBigLog()
    {
        return self::$_big_query_logs;
    }
// -------------------------------------------------------------------------------------
    public function close()
    {
        
    }
// -------------------------------------------------------------------------------------
    public function getLogCount()
    {
        return array_count( self::$_query_logs );
    }
// -------------------------------------------------------------------------------------
    public function getBigLogCount()
    {
        return array_count( self::$_big_query_logs );
    }
// -------------------------------------------------------------------------------------
    protected function _correctTableName($request)
    {
        if( !empty($this->_tables) ) {
            foreach($this->_tables as $key=>$val) {
                $modifed_name = str_replace($this->_config['prefix'], '', $val);
                $request      = str_replace( array(
                        'table_' . $modifed_name,
                        'tab_' . $modifed_name
                    ),
                    $this->_config['prefix'] . $modifed_name, 
                    $request
                );
            }
        }
        
        return $request;
    }
// -------------------------------------------------------------------------------------
    protected function _arrayToObject($_array)
    {
        if( array_count($_array) > 0 ) {
            $_ = new \stdClass();
            
            foreach($_array as $key=>$val) {
                if( $this->_isCorrectVariableName($key) ) {
                    $_->$key = $val;
                }
            }
            
            return $_;
        }
        
        return new \stdClass();
    }
// -------------------------------------------------------------------------------------
    protected function _isCorrectVariableName( $variable )
    {
        if( isset($variable)
         && preg_match('/^[a-z0-9\_]+$/i', $variable)
         && preg_match('/^[a-z\_]/i', $variable) )
        {
            return true;
        }
        
        return false;
    }
// -------------------------------------------------------------------------------------
    protected function _addRequestLog( $request )
    {
        if( isset($request) ) {
            self::$_query_logs[] = array(
                'REQUEST'  => $request,
                'DATETIME' => date('d.m.Y H:i:s'),
            );
        }
    }
// -------------------------------------------------------------------------------------
    protected function _addBigRequestLog( $request )
    {
        if( isset($request) ) {
            self::$_big_query_logs[] = array(
                'REQUEST'  => $request,
                'DATETIME' => date('d.m.Y H:i:s'),
            );
        }
    }
    
// -------------------------------------------------------------------------------------
    public static function __callStatic($method, $params)
    {
        $db = self::getInstance();
        
        if( method_exists($db, $name) ) {
            return call_user_func_array(array($db, $method), $params);
        }
        
        throw new \Exception('Unknown static method name ' . __CLASS__ . '::' . $method);
    }
}
