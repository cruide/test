<?php namespace App\Classes;

use App\Traits\SingletonTrait;
use App\Classes\Native;

class App
{
    use SingletonTrait;

    protected $input;
    protected $router;
    protected $cfg;
    protected $controller;
    protected $layout;

    public function __construct()
    {
        $this->layout = 'layout';
        $this->router = router();
        $this->input  = input();
        $this->cfg    = cfg('dbase');
        $controller   = $this->router->getControllerName();

        session();
        cookies();

        if( controller_exists($controller) ) {
            require(ACTIONS_DIR . DIR_SEP . "{$controller}.php");

            $className = '\\App\\Controllers\\' . $controller;

            if( class_exists($className) && is_subclass_of($className, '\\App\\Abstracts\\Controller') ) {
                try {
                    $this->controller = new $className();
                } catch( \Exception $e ) {
                    abort( $e->getMessage() . "\n" . $e->getFile() . "\n" . $e->getLine() );
                }

            } else {
                abort('Incorrect controller instance');
            }

        } else {
            abort('Not found, 404', 404);
        }
    }

    public function setLayout( $name )
    {
        $this->layout = $name;

        return $this;
    }

    public function execute()
    {
        Native::assign_global('base_url', BASE_URL);
        Native::assign_global('js_url'  , ASSETS_URL . '/js');
        Native::assign_global('css_url' , ASSETS_URL . '/css');

        $themplate = new Native( VIEWS_DIR );
        $method    = $this->router->getMethodName();

        if( !method_exists($this->controller, $method) ) {
            abort('Not found...', 404);
        }

        $params  = input()->getParams();
        $content = (array_count($params) > 0)
                       ? call_user_func_array([$this->controller, $method], $params)
                       : call_user_func([$this->controller, $method]);  
        
        $themplate->assign('content', $content);
        
        try {
            $themplate->display( $this->layout );
            
        } catch( \Exception $e ) {
            abort( $e->getMessage() );
        }
    }
}
