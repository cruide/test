<?php namespace App\Classes;

use App\Traits\RegistryTrait;
use App\Classes\stdObject;

class TableItem
{
    use RegistryTrait;
    
    protected $_tObject_table; 
    protected $_tObject_shadow;
    protected $table;
// -------------------------------------------------------------------------------------
    /**
    * Constructor of class
    * 
    * @param string $table
    * @param array $filterss
    * @return tObject
    */
    public function __construct($table = null, $filters = null)
    {
        if( !empty($table) && db()->hasTable( $table ) ) {
            $this->table = $table;
        }
        
        if( empty($this->table) || !db()->hasTable($this->table) ) {
            abort("Table `{$this->table}` not found");
        }
        
        $this->_tObject_table  = new stdObject();
        $this->_tObject_shadow = new stdObject();

        $this->_tObject_table->columns     = db()->getFieldsList($this->table);
        $this->_tObject_table->primary_key = db()->getPrimaryKey($this->table);
        $this->_tObject_table->name        = $this->table;

        if( empty($this->_tObject_table->primary_key) ) {
            abort("Undefined primary key into `{$this->table}`");
        }

        foreach($this->_tObject_table->columns as $key=>$val) {
            if( $val == 'integer' || $val == 'float' || $val == 'double' || $val == 'decimal' ) {
                $this->$key = $this->_tObject_shadow->$key = 0;
                
            } else {
                $this->$key = $this->_tObject_shadow->$key = '';
            }
        }

        if( count($filters) > 0 ) {
            $this->search($filters);
        }
        
        unset($key, $val, $data);
    }
// -------------------------------------------------------------------------------------
    public function hasColumn($name)
    {
        if( is_array($this->_tObject_table->columns) ) {
            return array_key_isset($name, $this->_tObject_table->columns);
        }
        
        return isset($this->_tObject_table->columns->$name);
    }
// -------------------------------------------------------------------------------------
    public static function find($table, $filter )
    {
        if( is_array($filter) ) {
            return new TableItem($table, $filter);
        }
        
        if( is_scalar($filter) ) {
            return new TableItem($table, array(
                db()->getPrimaryKey($table) => $filter,
            ));
        }
        
        return null;
    }
// -------------------------------------------------------------------------------------
    public static function delete($table, $id)
    {
        if( db()->hasTable($table) ) {
            return db()->delete($table, $id);
        }
        
        return false;
    }
// -------------------------------------------------------------------------------------
    public static function create($table, $data)
    {
        if( db()->hasTable($table) ) {
            $item = new static($table);
            
            foreach($data as $column=>$value) {
                if( $item->hasColumn($column) ) {
                    $item->$column = $value;
                }
            }
            
            if( $item->save() ) {
                return $item;
            }
        }
        
        return null;
    }
// -------------------------------------------------------------------------------------
    /**
    * Get data from table by filters
    * 
    * @param array $filters
    */
    public function search($filters)
    {
        if( empty($filters) || !is_array($filters) ) {
            return null;
        }
        
        $where_keys = $where_data = array();
        
        foreach($filters as $key=>$val) {
            if( array_key_isset($key, $this->_tObject_table->columns) ) {
                $where_keys[]       = "`{$key}` = :{$key}";
                $where_data[ $key ] = $val;
            }
        }
        
        $where = implode(' AND ', $where_keys);
        
        try {
            $_tmp  = db()->select($this->_tObject_table->name, array(
                'where'  => $where,
                'values' => $where_data,
                'limit'  => 1,
            ));
        } catch( \Exception $e ) {
            abort( $e->getMessage() );
        }
                     
        if( empty($_tmp) ) {
            return false;
        }
        
        foreach($_tmp as $key=>$val) {
            $this->$key = $this->_tObject_shadow->$key = $val;
        }
        
        return true;
    }
// -------------------------------------------------------------------------------------
    public function save()
    {
        $key_name = $this->_tObject_table->primary_key;

        if( !empty($this->_tObject_shadow->$key_name) ) {
            return $this->_tObject_update();
        }
        
        return $this->_tObject_insert();
    }
// -------------------------------------------------------------------------------------
    protected function _tObject_insert()
    {
        $primary_key_name = $this->_tObject_table->primary_key;

        if( !empty($this->_tObject_shadow->$primary_key_name) ) {
            return false;
        }
        
        $columns = array();
        $values  = array();
        
        foreach($this->_properties as $key=>$val) {
            if( is_scalar($val) && array_key_isset($key, $this->_tObject_table->columns) ) {
                if( ($key == $this->_tObject_table->primary_key && $val != '') || $key != $this->_tObject_table->primary_key ) {
                    $columns[]      = "`{$key}` = :{$key}";
                    $values[ $key ] = $val;
                }
            }
        }
        
        $setters = implode(', ', $columns);
        
        if( empty($setters) ) {
            return false;
        }

        try {
            db()->request("INSERT INTO `{$this->_tObject_table->name}` SET {$setters}", $values);
                
        } catch( \Exception $e ) {
            abort( $e->getMessage() );
        }
   
        foreach($this->_properties as $key=>$val) {
            if( is_scalar($val) && array_key_isset($key, $this->_tObject_table->columns) ) {
                if( ($key == $this->_tObject_table->primary_key && $val != '') || $key != $this->_tObject_table->primary_key ) {
                    $this->_tObject_shadow->$key = $val;
                }
            }
        }
        
        return $this->$primary_key_name = $this->_tObject_shadow->$primary_key_name = db()->lastInsertId();
    }
// -------------------------------------------------------------------------------------
    protected function _tObject_update()
    {
        $primary_key_name = $this->_tObject_table->primary_key;
        
        if( empty($this->_tObject_shadow->$primary_key_name) || $this->_tObject_shadow->$primary_key_name != $this->$primary_key_name ) {
            return false;
        }

        $columns = array();
        $values  = array();

        foreach($this->_properties as $key=>$val) {
            if( array_key_isset($key, $this->_tObject_table->columns) && $key != $primary_key_name && $val != $this->_tObject_shadow->$key ) {
                $columns[]      = "`{$key}` = :{$key}";
                $values[ $key ] = $val;
            }
        }
        
        $setters = implode(', ', $columns);
        $values['table_primary_key'] = $this->_tObject_shadow->$primary_key_name;

        if( empty($setters) ) {
            return true;
        }
        
        try {
            db()->prepare("UPDATE `{$this->_tObject_table->name}` SET {$setters} WHERE (`{$primary_key_name}` = :table_primary_key)")
                ->execute($values);
                
        } catch( \Exception $e ) {
            abort( $e->getMessage() );
        }
   
        foreach($this->_properties as $key=>$val) {
            if( is_scalar($val) && array_key_isset($key, $this->_tObject_table->columns) && $key != $primary_key_name && $this->$key != $this->_tObject_shadow->$key ) {
                $this->_tObject_shadow->$key = $val;
            }
        }
        
        return true;
    }
// -------------------------------------------------------------------------------------
    public function __destruct()
    {
        unset( $this->_tObject_shadow, $this->_tObject_table );
    }
}
