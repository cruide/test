<?php namespace App\Classes;

use App\Traits\SingletonTrait;

final class Router
{
    use SingletonTrait;

    private $controller;
    private $method;
    private $action;
    private $request;
    private $uri;
// -------------------------------------------------------------------------------------
    public function __construct()
    {
        global $_SERVER;
        
        $_REQUEST_URI = preg_replace( '/^\/\?/', '/', $_SERVER['REQUEST_URI']);
        $_REQUEST_URI = preg_replace( '/^\/+/', '/', $_REQUEST_URI);

        $superfluous = ['.html', '.htm', '.php5', '.php', '.php3', '.shtml', '.phtml', '.dhtml', '.xhtml', '.inc', '.cgi', '.pl','.xml', '.js'];
        $this->uri   = preg_replace( "#/+#s", '/', $_REQUEST_URI );
        $this->uri   = preg_replace( ["#/$#s", "#^/+#"], '', $this->uri );
        $this->uri   = $this->request = str_replace($superfluous, '', $this->uri);
        
        if( preg_match("%[^a-z0-9\/\-\.]%isu", $this->uri) ) {
            abort("Abnormal request: {$this->uri}");
        }

        if( empty($this->uri) ) {
            $this->controller = 'Index';
            $this->method     = 'index';
            $this->action     = snake_case($this->controller) . '/' . snake_case($this->method);

            return;
        }

        $do         = explode('/', $this->uri);
        $controller = ( isset($do[0]) ) ? $do[0] : null;
        $method     = ( isset($do[1]) ) ? $do[1] : null;

        if( empty($controller) || !is_action_name($controller) ) {
            $this->controller = 'Index';
            $this->method     = 'index';

        } else {
            $this->controller = studly_case($controller);

            if( empty($method) || !is_action_name($method) ) {
                $this->method = 'index';
            } else {
                $this->method = camel_case($method);
            }
        }
        
        $this->action = snake_case($this->controller, '-') . '/' . snake_case($this->method, '-');

        if( !controller_exists($this->controller) ) {
            abort('Incorrect action name...');
        }

        array_shift($do);
        array_shift($do);

        $this->_set_get_params($do);
    }
// -------------------------------------------------------------------------------------
    public function getAction()
    {
        return $this->action;
    }
// -------------------------------------------------------------------------------------
    public function getControllerName()
    {
        return $this->controller;
    }
// -------------------------------------------------------------------------------------
    public function getMethodName()
    {
        return $this->method;
    }
// -------------------------------------------------------------------------------------
    private function _set_get_params($_array = [])
    {
        global $_GET;

        if( array_count($_array) > 0 && is_even(count($_array)) ) {
            $_array_count = floor( count($_array) / 2 );

            for($i=0; $i/2 < $_array_count; $i+=2) {
                $_GET[ $_array[$i] ] = ( !empty($_array[$i+1]) ) ? $_array[$i+1] : '';
            }
        }
    }
}