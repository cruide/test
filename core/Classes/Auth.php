<?php namespace App\Classes;

use App\Classes\TableItem;
use App\Traits\SingletonTrait;

final class Auth
{
    use SingletonTrait;
    
    private $autorization, $user;
// -----------------------------------------------------------------------------
    public function __construct()
    {
        $this->autorization = false;
        $token              = cookies()->token;
        
        $obj = db('tab_users', [
            'session' => session()->id(true),
            'ip'      => get_ip_address(),
        ]);
        
        if( !empty($obj->id) ) {
            $obj->stamp = time();
            $obj->save();
            
            $this->autorization = true;
            $this->user         = $obj;
         
            unset($obj);
            
            if( !empty($token) && is_uuid($token) ) {
                cookies()->token = $token; 
            }
            
        } else if( !empty($token) && is_uuid($token) ) {
            $obj = db('tab_users', [
                'token' => $token,
            ]);
            
            if( !empty($obj->id) ) {
                $obj->stamp   = time();
                $obj->session = session()->id(true);
                $obj->ip      = get_ip_address();
                $obj->save();
                
                cookies()->token = $token;
                
                $this->autorization = true;
                $this->user         = $obj;
             
                unset($obj);
            }
        }
    }
// -----------------------------------------------------------------------------
    public function signin($email, $password)
    {
        if( !is_email($email) ) {
            return false;
        }
        
        $user = db('tab_users', [
            'email'    => $email,
            'password' => password_crypt($password),
        ]);
               
        if( !empty($user->id) ) {
            $user->session = session()->id(true);
            $user->stamp   = time();
            $user->ip      = get_ip_address();
            $user->save();
            
            $this->autorization = true;
            $this->user         = $user;
            
            return true;
        }
        
        return false;
    }
// -----------------------------------------------------------------------------
    public function signout()
    {
        if( $this->autorization ) {
            $this->user->session = '';
            $this->user->ip      = '';
            $this->user->save();

            $this->user         = null;
            $this->autorization = false;
            
            session()->destroy();
        }
    }
// -----------------------------------------------------------------------------
    public function user()
    {
        if( $this->autorization ) {
            return $this->user;
        }
        
        return null;
    }
// -----------------------------------------------------------------------------
    public function id()
    {
        if( $this->autorization ) {
            return $this->user->id;
        }
        
        return null;
    }
// -----------------------------------------------------------------------------
    public function check()
    {
        return $this->autorization;
    }
}
