<?php

define('ACTIONS_DIR' , ROOT . DIR_SEP . 'controllers');
define('INCLUDE_DIR' , ROOT . DIR_SEP . 'core');
define('SETTINGS_DIR', ROOT . DIR_SEP . 'settings');
define('VIEWS_DIR'   , ROOT . DIR_SEP . 'views');
define('ASSETS_URL'  , BASE_URL . '/assets');

define('CONTENT_TYPE_JSON' , 'Content-type: application/json; charset=utf-8');



