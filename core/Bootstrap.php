<?php

require('Constants.php');
require('Functions.php');

require('Traits/RegistryTrait.php');
require('Traits/SingletonTrait.php');

require('Abstracts/Controller.php');

require('Classes/stdObject.php');
require('Classes/Input.php');
require('Classes/Router.php');
require('Classes/Cookies.php');
require('Classes/Session.php');
require('Classes/Native.php');
require('Classes/App.php');
require('Classes/PDB.php');
require('Classes/TableItem.php');
require('Classes/Auth.php');

app()->execute();
