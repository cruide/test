<?php namespace App\Traits;
/**
* Registry trait pattern
* 
* @author Alex Tisch
*/
  
trait RegistryTrait
{
	protected $_properties = [];
	
	/**
	* Getter for $this->_properties
	* 
	* @param strung $name
	*/
	public function __get($name)
	{
		if( isset($this->_properties[$name]) && array_key_exists($name, $this->_properties) ) {
			return $this->_properties[ $name ];
		}
		
		return null;
	}
	
	/**
	* Setter for $this->_properties
	* 
	* @param string $name
	* @param mixed $value
	*/
	public function __set($name, $value)
	{
		$this->_properties[ $name ] = $value;
	}
	
	/**
	* Isset for $this->_properties
	* 
	* @param string $name
	* @return bool
	*/
	public function __isset($name)
	{
		return isset($this->_properties[$name]);
	}
	
	/**
	* Unset for $this->_properties
	* 
	* @param string $name
	*/
	public function __unset($name)
	{
		unset($this->_properties[$name]);
	}
    
    public function serialize()
    {
        return serialize($this->_properties);
    }
    
    public function toJson()
    {
        return json_encode($this->_properties);
    }
    
    public function toArray()
    {
        $publics = function($obj) {
            return get_object_vars($obj);
        };
        
        return $publics($this);
    }
}



