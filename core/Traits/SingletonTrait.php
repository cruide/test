<?php namespace App\Traits;
/**
* Singleton trait pattern
* 
* @author Alex Tisch
*/
  
trait SingletonTrait
{
	protected static $_instance;
	
	/**
	* Return object instance of class
	*/
	public static function getInstance()
	{
		if( static::$_instance == null ) {
			static::$_instance = new static();
		}
		
		return static::$_instance;
	}
}
