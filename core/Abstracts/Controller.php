<?php namespace App\Abstracts;

abstract class Controller
{
    protected $ui;

    public function __construct()
    {
        $this->ui = new \App\Classes\Native( VIEWS_DIR );
    }

    abstract public function index();
    
    protected function view($view_name, array $params = [])
    {
        if( count($params) ) {
            foreach($params as $name=>$value) {
                $this->ui->assign($name, $value);
            }
        }
        
        return $this->ui->fetch($view_name);
    }
    
    protected function json( $data )
    {
        header( CONTENT_TYPE_JSON );
        
        exit( json_encode($data) );
    }
}
