<?php

  /**
  * Corretion for directory path
  *
  * @param string $dir
  * @return string
  */
  function path_correct( $dir, $end_slash = false )
  {
      if( empty($dir) ) return $dir;

      $dir = str_replace(':', '||'   , $dir);
      $dir = str_replace('\\\\', '::', $dir);
      $dir = str_replace('\\', '::'  , $dir);
      $dir = str_replace('//', '::'  , $dir);
      $dir = str_replace('/', '::'   , $dir);
      $dir = str_replace('::', '/'   , $dir);
      $dir = str_replace('||', ':'   , $dir);

      if( $end_slash && !preg_match("#(.*)\/$#is" , $dir, $tmp) ) {
          $dir = $dir . '/';
      }

      return $dir;
  }

  /**
  * Check action
  *
  * @param string $action
  * @return bool
  */
  function controller_exists( $action )
  {
      if( is_file( ACTIONS_DIR . DIR_SEP . "{$action}.php" ) ) {
          return true;
      }

      return false;
  }
  /**
  * Return IP adddress of client
  *
  */
  function get_ip_address()
  {
      global $_SERVER;

      if( !empty($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['HTTP_CLIENT_IP']) ) {
          $ipaddr = $_SERVER['HTTP_CLIENT_IP'];
      } else if( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
          $ipaddr = $_SERVER['HTTP_CLIENT_IP'];
      } else if( !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
          $ipaddr = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } else {
          $ipaddr = $_SERVER['REMOTE_ADDR'];
      }

      if( $ipaddr === false ) {
          return '0.0.0.0';
      }

      if( strstr($ipaddr, ',') ) {
          $x = explode(',', $ipaddr);
          $ipaddr = end( $x );
      }

      if( filter_var($ipaddr, FILTER_VALIDATE_IP) === false ) {
          $ipaddr = '0.0.0.0';
      }

      return $ipaddr;
  }

  function escape($str, $escapemethod = 'htmlspecialchars')
  {
      if( array_count($str) > 0 ) {
          $_ = [];
          foreach($str as $key=>$val) {
              $_[ $key ] = escape($val);
          }
          return $_;
      } else if( is_string($str) ) {
          if( in_array($escapemethod, ['htmlspecialchars', 'htmlentities']) ) {
              return call_user_func($escapemethod, $str, ENT_COMPAT, 'UTF-8');
          }

          return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
      }

      return $str;
  }

  function unescape($str)
  {
      if( is_array($str) && count($str) ) {
          $_ = [];
          foreach($str as $key=>$val) {
              $_[ $key ] = unescape($val);
          }
          return $_;
      } else if( is_string($str) ) {
          return htmlspecialchars_decode($str, ENT_QUOTES);
      }

      return $str;
  }

  /**
  * проверка на четность
  */
  function is_even($num)
  {
      if( !is_numeric($num) ) return false;
      if( !preg_match('/[\.]/s', $num) ) {
          return ( fract($num) & 1 ) ? false : true;
      } else {
          return ( $num & 1 ) ? false : true;
      }
  }

  /**
  * Getting the number of entries in the array.
  * If this is not an array, it returns 0
  *
  * @param array $_
  * @param mixed $mode
  * @return integer
  */
  function array_count($_, $mode = null)
  {
      return (!is_array($_)) ? 0 : count($_, $mode);
  }

  /**
  * A faster version of the check for key in the array
  *
  * @param string $key
  * @param array $_array
  * @return bool
  */
  function array_key_isset($key, $_array)
  {
      if( !is_array($_array) ) return false;

      return (isset($_array[ $key ]) || array_key_exists($key, $_array));
  }

  /**
  * Генератор соли
  *
  * @return string $lenght
  */
  function salt_generation( $lenght = 18 )
  {
      return substr( sha1(mt_rand()), 0, $lenght );
  }

  /**
  * Шифрование пароля
  *
  * @param string $password
  * @param string $salt
  */
  function password_crypt( $password, $salt = null )
  {
      if( empty($password) ) { return false; }
      if( null == $salt )   { $salt = 'testtest'; }

      return crypt( md5($password), '$1$' . $salt . '$' );
  }

  /**
  * Get settings from INI files
  *
  * @param string $name
  */
  function cfg($name)
  {
      static $settings;

      if( empty($settings) ) {
          $settings = new \stdClass();
      }

      if( isset($settings->$name) ) {
          return $settings->$name;
      }

      if( isset($name) && is_file(SETTINGS_DIR . DIR_SEP . (string)$name . '.ini') ) {
          try {
              $tmp = parse_ini_file( SETTINGS_DIR . DIR_SEP . (string)$name . '.ini', true );
          } catch( \Exception $e ) {
              exit( $e->getMessage() );
          }

          if( array_count($tmp) > 0 ) {
              $ini = new \stdClass();

              foreach($tmp as $key=>$val) {
                  if( array_count($val) > 0 ) {
                      $section = str_replace( ['.', '-'], '_', strtolower($key) );
                      $data    = new \stdClass();

                      foreach($val as $k=>$v) {
                          if( is_varible_name($k) ) {
                              $data->$k = $v;
                          }
                      }

                      $ini->$section = $data;
                  } else {
                      $varible       = str_replace( '.', '_', strtolower($key) );
                      $ini->$varible = $val;
                  }
              }

              $settings->$name = $ini;
              unset($key, $val, $section, $k, $v, $data, $tmp);

              return $ini;
          }
      }

      return new \stdClass();
  }

  /**
  * Check for ajax request
  *
  */
  function is_ajax()
  {
      global $_SERVER;

      if( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) {
          return true;
      } else if( !empty($_SERVER['X_REQUESTED_WITH']) && $_SERVER['X_REQUESTED_WITH'] == 'XMLHttpRequest' ) {
          return true;
      } else if( !empty($_SERVER['HTTP_ACCEPT']) && (false !== strpos($_SERVER['HTTP_ACCEPT'], 'text/x-ajax')) ) {
          return true;
      }

      return false;
  }

  /**
  * Aliases
  */
  function input()
  {
      return \App\Classes\Input::getInstance();
  }

  function session()
  {
      return \App\Classes\Session::getInstance();
  }

  function cookies()
  {
      return \App\Classes\Cookies::getInstance();
  }

  function router()
  {
      return \App\Classes\Router::getInstance();
  }

  function app()
  {
      return \App\Classes\App::getInstance();
  }

  function auth()
  {
      return \App\Classes\Auth::getInstance();
  }
  
  function db($table = null, $filters = null)
  {
      if( !$table ) {
          return \App\Classes\PDB::getInstance();
      }
      
      return new \App\Classes\TableItem($table, $filters);
  }

  function studly_case( $value )
  {
      static $cache;

      if( isset($cache[$value]) ) {
          return $cache[ $value ];
      }

      return $cache[ $value ] = str_replace(' ', '', ucwords( str_replace(['-', '_'], ' ', $value) ));
  }

// ------------------------------------------------------------------------------

  function camel_case( $value )
  {
      return lcfirst( studly_case($value) );
  }

// ------------------------------------------------------------------------------
  /**
   * Convert a string to snake case.
   *
   * @param  string  $value
   * @param  string  $delimiter
   * @return string
   */
  function snake_case($value, $delimiter = '_', $convert_spaces = false)
  {
      static $cache;

      if( !is_array($cache) ) $cache = [];

      $key = $value . $delimiter;

      if( isset($cache[ $key ]) ) return $cache[ $key ];

      if( !ctype_lower($value) ) {
          $value = !$convert_spaces ? $value = preg_replace('/\s+/', '', $value)
                                    : str_replace(' ', $delimiter, $value);

          $value = strtolower(
              preg_replace('/(.)(?=[A-Z])/', '$1' . $delimiter, $value)
          );
      }

      return $cache[ $key ] = $value;
  }

  function is_action_name($str)
  {
      if( empty($str) || !is_scalar($str) || preg_match('/^[0-9_\-]/', (string)$str) || !preg_match('/^[a-z0-9_\-]+$/i', (string)$str) ) {
          return false;
      }

      return true;
  }

  function is_varible_name($str)
  {
      if( !isset($str)
       || !is_scalar($str)
       || !preg_match('/^[a-z0-9\_]+$/i', (string)$str)
       || preg_match('/^[0-9]/', (string)$str) )
      {
          return false;
      }

      return true;
  }

  function is_email($str)
  {
      return filter_var($str, FILTER_VALIDATE_EMAIL);
  }

  function is_alphanum($str)
  {
      if( !isset($str) || !is_scalar($str) || !preg_match('/^[a-zA-Zа-яА-ЯЁё0-9]+$/u', (string)$str) ) {
          return false;
      }

      return true;
  }

  function is_date($str)
  {
      if( empty($str) || !is_scalar($str) || !preg_match('%^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)[0-9]{2}+$%', (string)$str) ) {
          return false;
      }

      return true;
  }

  function is_url($str)
  {
      return filter_var($str, FILTER_VALIDATE_URL);
  }

  function is_alpha($str)
  {
      if( !isset($str) || !is_scalar($str) || !preg_match('/^[a-zA-Zа-яА-ЯЁё]+$/u', (string)$str) ) {
          return false;
      }

      return true;
  }

  function is_ipaddress($str)
  {
      return filter_var($str, FILTER_VALIDATE_IP);
  }
  
  function is_name($str)
  {
      if( !isset($str) || !is_scalar($str) || !preg_match('/^[a-zA-Zа-яА-ЯЁё0-9\s\']+$/u', (string)$str) ) {
          return false;
      }

      return true;
  }

  function redirect($href = null)
  {
      if( !$href ) {
          header('Location: ' . BASE_URL);
          exit();
      }
      
      header( 'Location: ' . make_url($href), true, 301);
      exit();
  }

  /**
  * Make correct URL
  * 
  * @param string $url
  * @param bool $add_suf
  */
  function make_url($url, $add_suf = true) 
  {
      if( empty($url) || !is_scalar($url) ) return false;
      if( preg_match('#^(http|https|ftp)://(.*?)#i', $url) ) return $url;
      
      if( !preg_match('/^\//', $url) ) {
          $url = '/' . $url;
      }
      
      return BASE_URL . $url;
  }
  
  /**
  * Generate a unique key
  */
  function uuid()
  {
      return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
          mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
          mt_rand( 0, 0x0fff ) | 0x4000,
          mt_rand( 0, 0x3fff ) | 0x8000,
          mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
      );
  }

// ------------------------------------------------------------------------------
  /**
  * Checking the unique key
  * 
  * @param string $uuid
  */
  function is_uuid($uuid)
  {
      return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
  }
  
  function abort($message, $code = 500)
  {
      $status = get_http_status($code);
      
      if( !headers_sent() ) {
          header('HTTP/1.1 ' . $status);
          header('Status: ' . $status);
      }
      
      exit( $message );
  }

  /**
  * Get ststus by code number
  * 
  * @param integer $code
  */
  function get_http_status($code)
  {
      if( !is_numeric($code) ) { 
          return false;
      }
    
      $_HTTP_STATUS = [
            100 => '100 Continue',
            101 => '101 Switching Protocols',
            102 => '102 Processing',
            200 => '200 OK',
            201 => '201 Created',
            202 => '202 Accepted',
            203 => '203 Non-SB_Authoritative Information',
            204 => '204 No Content',
            205 => '205 Reset Content',
            206 => '206 Partial Content',
            207 => '207 Multi Status',
            226 => '226 IM Used',
            300 => '300 Multiple Choices',
            301 => '301 Moved Permanently',
            302 => '302 Found',
            303 => '303 See Other',
            304 => '304 Not Modified',
            305 => '305 Use Proxy',
            306 => '306 (Unused)',
            307 => '307 Temporary Redirect',
            400 => '400 Bad Request',
            401 => '401 Unauthorized',
            402 => '402 Payment Required',
            403 => '403 Forbidden',
            404 => '404 Not Found',
            405 => '405 Method Not Allowed',
            406 => '406 Not Acceptable',
            407 => '407 Proxy nCore_Authentication Required',
            408 => '408 Request Timeout',
            409 => '409 Conflict',
            410 => '410 Gone',
            411 => '411 Length Required',
            412 => '412 Precondition Failed',
            413 => '413 Request Entity Too Large',
            414 => '414 Request-URI Too Long',
            415 => '415 Unsupported Media Type',
            416 => '416 Requested Range Not Satisfiable',
            417 => '417 Expectation Failed',
            420 => '420 Policy Not Fulfilled',
            421 => '421 Bad Mapping',
            422 => '422 Unprocessable Entity',
            423 => '423 Locked',
            424 => '424 Failed Dependency',
            426 => '426 Upgrade Required',
            449 => '449 Retry With',
            500 => '500 Internal Server Error',
            501 => '501 Not Implemented',
            502 => '502 Bad Gateway',
            503 => '503 Service Unavailable',
            504 => '504 Gateway Timeout',
            505 => '505 HTTP Version Not Supported',
            506 => '506 Variant Also Varies',
            507 => '507 Insufficient Storage',
            509 => '509 Bandwidth Limit Exceeded',
            510 => '510 Not Extended',
      ];
    
      if( !empty($_HTTP_STATUS[$code]) ) {
          return $_HTTP_STATUS[$code];
      }
    
      return false;
  }
  